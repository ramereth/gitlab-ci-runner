#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

execute 'Create root user access token' do
  command %(sudo gitlab-rails runner "token = User.find_by_username('root').personal_access_tokens.create(scopes: [:api], name: 'Automation token', expires_at: '#{Time.now + (3600 * 168)}'); token.set_token('ABC123'); token.save!")
  only_if %q(exit $(sudo gitlab-rails runner "puts (User.find_by_username('root').personal_access_tokens.select { |t| t.name = 'Automation token' }.count)"))
end

execute 'Create test group' do
  command %(sudo gitlab-rails runner "group = Group.create(name: 'test-group', path: 'test-group'); user = User.find_by_username('root'); group.add_owner(user); group.save!")
  not_if %q(exit $(sudo gitlab-rails runner "puts Group.find_by(name: 'test-group') ? 0 : 1"))
end
