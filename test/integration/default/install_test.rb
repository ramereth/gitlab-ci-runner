# InSpec test for gitlab_ci_runner::package

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe package('gitlab-runner') do
  it { should be_installed }
end

if os.redhat? && os.release.to_i.eql?(8)
  describe processes('gitlab-runner') do
    its('states') { should eq ['Ssl'] }
  end
else
  describe service('gitlab-runner') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

describe file('/etc/gitlab-runner/config.toml') do
  it { should exist }
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should_not be_directory }
  its('content') { should match /concurrent = 5/ }
  its('content') { should match /check_interval = 3/ }
end
