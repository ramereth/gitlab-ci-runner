# InSpec test for gitlab_ci_runner::runner

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe file('/etc/gitlab-runner/config.toml') do
  it { should exist }
  its('type') { should cmp 'file' }
  it { should be_file }
  it { should_not be_directory }
  its('content') { should_not match /name = "\d+: test runner c&d"/ }
  its('content') { should match /name = "\d+: test runner authentication_token"/ }
  its('content') { should match /limit = 5/ }
  its('content') { should match /executor = "shell"/ }
  its('content') { should match %r{url = "http:\/\/kitchen-gitlab-ce"} }
end
