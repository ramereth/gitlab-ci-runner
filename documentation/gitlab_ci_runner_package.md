# gitlab_ci_runner_package

[Back to resource list](../README.md#resources)

Manage the installation of Gitlab CI runner, all settings are optional.

## Actions

- `:install`
- `:upgrade`
- `:uninstall`

## Usage

### Example

```ruby
gitlab_ci_runner_package 'gitlab-runner' do
  repo_url 'http://specify.custom.repo'
  gpgkey 'http://specify.custom.repo/gpgkey'
  gpgcheck false
  version '12.7.1'
  tls_verify false
  timeout 5
  package_options(
    'retries': 3,
    'retry_delay': 5
  )
  action :install
end
```

The full resource options list is available in [resources/global_config.rb](../resources/package.rb).
