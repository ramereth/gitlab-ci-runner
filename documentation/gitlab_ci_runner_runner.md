# gitlab_ci_runner_runner

[Back to resource list](../README.md#resources)

## Actions

- `:create`, `:register` - Create a runner using the supplied authentication if provided, otherwise create or register a new runner.
- `:update` - Update settings for a previously registered runner
- `:unregister` - Unregister a runner and remove its configuration

## Properties

### General

| Property               | Optional?       | Type   | Description                                                      |
| ---------------------- | --------------- | ------ | ---------------------------------------------------------------- |
| `url`                  | Register: No    | String | Gitlab server URL.                                               |
|                        | Unregister: Yes |        |                                                                  |
| `registration_token`   | Register: No    | String | Gitlab server legacy runner registration token.                  |
|                        | Unregister: Yes |        |                                                                  |
| `authentication_token` | Yes             | String | Pre-created runner authentication token.                         |
| `administration_token` | Yes             | String | Token with API access for an administrator on the gitlab server. |
| `config`               | Yes             | String | Gitlab runner configuration file path.                           |

### Runner Options

| Property      | Optional? | Type           | Description                                                                              |
| ------------- | --------- | -------------- | ---------------------------------------------------------------------------------------- |
| `runner_type` | Yes       | String, Symbol | Gitlab runner type to create. Options are: `instance_type`, `group_type`, `project_type` |
| `group_id`    | Yes       | Integer        | Group ID for a `group_type` runner.                                                      |
| `project_id`  | Yes       | Integer        | Group ID for a `project_type` runner.                                                    |

### Server Options

| Property          | Optional? | Type   | Description                                                                                            |
| ----------------- | --------- | ------ | ------------------------------------------------------------------------------------------------------ |
| `description`     | Yes       | String | Server runner description.                                                                             |
| `active`          | Yes       | Bool   | The state of a runner; can be set to `true` or `false`, paused runners don't accept new jobs.          |
| `tag_list`        | Yes       | Array  | The list of tags for a runner; put array of tags, that should be finally assigned to a runner.         |
| `run_untagged`    | Yes       | Bool   | Flag indicating the runner can execute untagged jobs.                                                  |
| `locked`          | Yes       | Bool   | Flag indicating the runner is locked When a runner is locked, it cannot be assigned to other projects. |
| `access_level`    | Yes       | String | The access_level of the runner; `not_protected` or `ref_protected`                                     |
| `maximum_timeout` | Yes       | Int    | Maximum timeout set when this Runner will handle the job.                                              |

### Local option

| Property  | Optional? | Type | Description                                                                                              |
| --------- | --------- | ---- | -------------------------------------------------------------------------------------------------------- |
| `options` | Yes       | Hash | Runner configuration options. <https://docs.gitlab.com/runner/configuration/advanced-configuration.html> |

## Usage

Register or Unregister runners.

### Example 1 - Register new runner

Register a default runner to <http://gitlab-ci.myinstance>.

```ruby
gitlab_ci_runner_runner 'my runner' do
  registration_token '1234567890'
  administrative_token '987654321'
  url 'http://gitlab-ci.myinstance'
  tag_list [ "testing-1", "testing-2" ]
  options({
    'executor' => 'shell'
  })
end
```

### Example 2 - Register and update

Register a runner and update it's configuration once registered. Runners that are already registered will have their configuration updated to match the supplied options. Empty hash values will be compacted (removed).

If an administrative token with suitable permissions has been provided the server runner configuration will also be updated to match the resource.

```ruby
gitlab_ci_runner_runner 'my runner' do
  registration_token '1234567890'
  administrative_token '987654321'
  url 'http://gitlab-ci.myinstance'
  description 'Testing Docker Runner 1'
  options({
    "executor" => "docker",
    "docker" => {
      "image" => "centos",
      "privileged" => false,
      "disable_entrypoint_overwrite" => false,
      "oom_kill_disable" => false,
      "disable_cache" => false,
      "volumes" => [
        "/cache",
        "/etc/chef:/etc/chef:ro"
      ],
      "shm_size" => 0
    },
    "cache" => {
      "s3" => {},
      "gcs" => {}
    },
    "custom_build_dir" => {
      "enabled" => true
    },
    "tag_list" => [
      "docker-test-1"
    ]
  })
  action [ :register, :update ]
end
```

Will generate this configuration:

```toml

[[runners]]
executor = "docker"
name = "Testing Docker Runner 1"
token = "9dK52sYkuXeVudzw3HuH"
url = "http://gitlab-ci.myinstance"

[runners.docker]
disable_cache = false
disable_entrypoint_overwrite = false
image = "centos"
oom_kill_disable = false
privileged = false
shm_size = 0
volumes = ["/cache","/etc/chef:/etc/chef:ro"]

[runners.custom_build_dir]
enabled = true
```

The full resource options list is available in [resources/runner.rb](../resources/runner.rb).

### Example 3 - Create a new instance runner

This will create a new instance level runner using the new runner registration workflow.

```ruby
gitlab_ci_runner_runner 'instance_runner' do
  sensitive false
  url 'http://kitchen-gitlab-ce'
  administration_token 'ABC123'
  http_options(
    'read_timeout' => 15,
    'verify_mode' => OpenSSL::SSL::VERIFY_NONE
  )
  tag_list %w(instance_tag)
  locked true
  options(
    'limit' => 3,
    'executor' => 'shell'
  )
  retries 1

  action :create
end
```

### Example 4 - Create a new group scoped runner

This will create a new group scoped runner using the new runner registration workflow.

```ruby
gitlab_ci_runner_runner 'group_runner' do
  sensitive false
  url 'http://kitchen-gitlab-ce'
  administration_token 'ABC123'
  runner_type 'group_type'
  group_id 2
  http_options(
    'read_timeout' => 15,
    'verify_mode' => OpenSSL::SSL::VERIFY_NONE
  )
  tag_list %w(group_tag)
  options(
    'limit' => 3,
    'executor' => 'shell'
  )
  retries 1

  action :create
end
```

### Example 5 - Create a runner using a pre-supplied authentication token

This will create a runner using the pre-supplied authentication token, no runner creation will take place on the gitlab server.

```ruby
gitlab_ci_runner_runner 'pre_defined_runner' do
  sensitive false
  url 'http://kitchen-gitlab-ce'
  authentication_token 'XYZ789'
  administration_token 'ABC123'
  http_options(
    'read_timeout' => 15,
    'verify_mode' => OpenSSL::SSL::VERIFY_NONE
  )
  tag_list %w(group_tag)
  options(
    'limit' => 3,
    'executor' => 'shell'
  )
  retries 1

  action :create
end
```
