# gitlab_ci_runner_global_config

[Back to resource list](../README.md#resources)

Manage the global options of Gitlab CI runner, all settings are optional.

## Actions

- `:set` - Get runner global configuration options

## Properties

| Property               | Optional?       | Type   |
|------------------------|-----------------|--------|
| `config`               | Yes             | String |
| `concurrent`           | Yes             | Int    |
| `log_level`            | Yes             | String |
| `log_format`           | Yes             | String |
| `check_interval`       | Yes             | Int    |
| `sentry_dsn`           | Yes             | Bool   |
| `listen_address`       | Yes             | String |
| `options`              | Yes             | Hash   |

Manage the global options of Gitlab CI runner, all settings are optional.

```ruby
gitlab_ci_runner_global_config '/etc/gitlab-runner/config.toml' do
  concurrent 5
  log_level info
  log_format text
  check_interval 0
  sentry_dsn false
  listen_address 127.0.0.1
  session_server{
    "listen_address" => "0.0.0.0:8093",
    "advertise_address" => "runner-host-name.tld:8093",
    "session_timeout"=> 1800
  }
end
```

## Usage

### Example

```ruby
gitlab_ci_runner_global_config '/etc/gitlab-runner/config.toml' do
  concurrent 5
  log_level info
  log_format text
  check_interval 0
  sentry_dsn false
  listen_address '127.0.0.1'
  session_server(
    "listen_address" => "0.0.0.0:8093",
    "advertise_address" => "runner-host-name.tld:8093",
    "session_timeout"=> 1800
  )
end
```

The full resource options list is available in [resources/global_config.rb](../resources/global_config.rb).
