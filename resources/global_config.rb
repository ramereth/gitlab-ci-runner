#
# Cookbook:: gitlab-ci-runner
# Resource:: global_config
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

unified_mode true

include GitlabCiRunner::Cookbook::Helpers
include GitlabCiRunner::Cookbook::TomlHelpers

property :config_file, String,
          default: lazy { default_gitlab_runner_config_file },
          description: 'Configuration file location'

property :cookbook, String,
          default: 'gitlab-ci-runner',
          description: 'Cookbook to source configuration file template from'

property :template, String,
          default: 'gitlab-runner/config.toml.erb',
          description: 'Template to use to generate the configuration file'

property :owner, String,
          default: lazy { default_gitlab_runner_user },
          description: 'Owner of the generated configuration file'

property :group, String,
          default: lazy { default_gitlab_runner_group },
          description: 'Group of the generated configuration file'

property :mode, String,
          default: '0600',
          description: 'Filemode of the generated configuration file'

property :sensitive, [true, false],
          default: true,
          desired_state: false

property :concurrent, Integer

property :log_level, String

property :log_format, String

property :check_interval, Integer

property :sentry_dsn, [true, false]

property :listen_address, String

property :options, Hash

property :session_server, Hash

load_current_value do |new_resource|
  current_value_does_not_exist! unless ::File.exist?(new_resource.config_file)

  if ::File.exist?(new_resource.config_file)
    owner ::Etc.getpwuid(::File.stat(new_resource.config_file).uid).name
    group ::Etc.getgrgid(::File.stat(new_resource.config_file).gid).name
    mode ::File.stat(new_resource.config_file).mode.to_s(8)[-4..-1]
  end

  current_config = toml_load(new_resource.config_file).select { |k, _| !k.eql?('runners') }
  GitlabCiRunner::Cookbook::RunnerHelpers::GLOBAL_CONFIG_PROPERTIES.each { |property| send(property, current_config.fetch(property.to_s, nil)) }
end

action_class do
  include GitlabCiRunner::Cookbook::ResourceHelpers
  include GitlabCiRunner::Cookbook::RunnerHelpers
end

action :set do
  config_resource_init

  converge_if_changed do
    config_resource_global.merge!(
      compact_hash!(GitlabCiRunner::Cookbook::RunnerHelpers::GLOBAL_CONFIG_PROPERTIES.map { |p| [p.to_s, new_resource.send(p).dup] }.to_h)
    )
  end
end
