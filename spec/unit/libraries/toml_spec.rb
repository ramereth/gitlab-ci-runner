#
# Cookbook:: gitlab-ci-runner
# Spec:: toml_spec
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'spec_helper'

describe 'GitlabCiRunner::TomlHelpers' do
  let(:dummy_class) { Class.new { include GitlabCiRunner::Cookbook::TomlHelpers } }

  describe '.toml_generate' do
    test_hash = {
      'test' => 'hash',
      'subhash' => {
        'one' => 1,
        'two' => 'two',
        'three' => 3.0,
      },
    }

    str = "test = \"hash\"\n[subhash]\none = 1\nthree = 3.0\ntwo = \"two\"\n"
    context 'given configuration hash' do
      it 'returns string output of toml file' do
        expect(
          dummy_class.new.toml_dump(test_hash)
        ).to be_a(String)

        expect(
          dummy_class.new.toml_dump(test_hash)
        ).to eql(str)
      end
    end
  end

  describe '.toml_load' do
    before(:each) do
      allow(File).to receive(:file?).and_call_original
      allow(TomlRB).to receive(:load_file).and_call_original
    end

    context 'given empty or no-config file' do
      it 'returns empty hash' do
        allow(File).to receive(:file?).with(/toml/).and_return(true)
        allow(TomlRB).to receive(:load_file).with(/toml/)
                                            .and_raise(NoMethodError)

        expect(
          dummy_class.new.toml_load(
            '/etc/gitlab-runner/config.toml'
          )
        ).to be_a(Hash)
        expect(
          dummy_class.new.toml_load(
            '/etc/gitlab-runner/config.toml'
          )
        ).to eql({})
      end
    end
  end
end
