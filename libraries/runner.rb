#
# Cookbook:: gitlab-ci-runner
# Library:: runner
#
# Copyright:: 2020, Ben Hughes <bmhughes@bmhughes.co.uk>, All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'json'

require_relative '_common'
require_relative 'api'
require_relative 'resource'
require_relative 'toml'

module GitlabCiRunner
  module Cookbook
    # Helper methods for configuring gitlab-runner via chef
    module RunnerHelpers
      include GitlabCiRunner::Cookbook::ApiHelpers
      include GitlabCiRunner::Cookbook::CommonHelpers
      include GitlabCiRunner::Cookbook::TomlHelpers

      GLOBAL_CONFIG_PROPERTIES ||= %i(check_interval concurrent listen_address log_level log_format sentry_dsn session_server).freeze
      RUNNER_EXECUTOR_REGEXP ||= /(docker|parallels|virtualbox|ssh|machine|kubernetes)(_|-)/
      RUNNER_LOCAL_OPTIONS ||= %i(url).freeze
      RUNNER_LOCAL_REMOVE ||= %w(registration_token).freeze
      RUNNER_LOCAL_PRESERVE ||= %w(name token).freeze

      private_constant :RUNNER_EXECUTOR_REGEXP, :RUNNER_LOCAL_REMOVE, :RUNNER_LOCAL_PRESERVE

      ##
      ## load_current_value Helpers
      ##
      def runner_config_present?(name = new_resource.name, config_file = new_resource.config_file)
        !nil_or_empty?(runner_get_url_token(name, config_file))
      end

      def runner_local_config(name = new_resource.name, config_file = new_resource.config_file)
        return unless ::File.exist?(config_file)

        local_runner_config = runners_config_merged(config_file).filter { |r| runner_split_id_name(r['name']).last.eql?(name) }
        return if nil_or_empty?(local_runner_config)

        raise "Expected a single config to be filtered for runner name #{name}, got #{local_runner_config.count} results:\n#{pp(local_runner_config)}\n" unless local_runner_config.one?

        local_runner_config = local_runner_config.first
        log_chef(:debug, "Got runner configuration for runner #{local_runner_config['name']}.")

        local_runner_config
      end

      # Get the runner server properties relevant for the registration type
      def runner_server_properties
        properties = case runner_registration_type
                     when :create, :supplied_token
                       %i(access_level paused description group_id locked maintenance_note maximum_timeout project_id run_untagged runner_type tag_list)
                     when :register
                       %i(access_level paused description locked maintenance_note maximum_timeout run_untagged tag_list)
                     end

        %i(group_id project_id runner_type).each { |p| properties.delete(p) } if action.eql?(:update)

        properties
      end

      # Get server property value from resource property or the `options` hash property
      def server_property_value(option)
        new_resource.send(option) || new_resource.options.fetch(option.to_s, nil)
      end

      # Return a registered runners server ID number
      def runner_id(name = new_resource.name, config_file = new_resource.config_file)
        id, n = runner_split_id_name(runner_local_config(name, config_file)['name'])

        unless id.nil?
          log_chef(:info, "Found ID #{id} for registered runner #{n}.")
          return id
        end

        log_chef(:warn, "No ID found for runner #{name}.")
        nil
      end

      # Get the URL and token for a runner
      def runner_get_url_token(name = new_resource.name, config_file = new_resource.config_file, id = nil)
        rc = runner_local_config(name, config_file)

        if nil_or_empty?(rc)
          log_chef(:debug, "Runner #{name} not found.")
          return
        end

        split_id, split_name = runner_split_id_name(rc['name'])

        if split_name.eql?(name) && (split_id.eql?(id) || id.nil?)
          log_chef(:info, "Found runner #{split_name} ID #{split_id} with token #{rc['token']} registered at url #{rc['url']}.")
          return [rc['url'], rc['token']]
        end

        log_chef(:debug, "Runner #{name} found but did not match provided ID.")
        nil
      end

      # Get the server state for a registered runner
      def runner_server_config(name = new_resource.name, config_file = new_resource.config_file, token = new_resource.administration_token, http_options = new_resource.http_options)
        log_chef(:info, "Getting config for runner #{name} from host #{url}")
        id = runner_id(name, config_file)
        url, = runner_get_url_token(name, config_file, id)

        response = api_runner_get(url, http_options, token, id)
        raise "#{response.code} Error getting runner configuration: #{api_error_message_output(response)}" unless api_response_is?(response, Net::HTTPOK)

        config = JSON.parse(response.body)
        log_chef(:debug) { "Config for runner '#{name}' from host #{url}:\n#{config}\n" }

        config
      end

      def runner_server_token_id(url = new_resource.url, token = new_resource.authentication_token, http_options = new_resource.http_options)
        log_chef(:info, "Getting config for runner with token #{token} from host #{url}")

        config = JSON.parse(api_runner_token_data(url, http_options, token).body)
        log_chef(:debug, "Token data for token #{token} from host #{url}:\n#{config}\n")

        config.fetch('id')
      end

      ##
      ## General resource helpers
      ##

      # Update an in-place local configuration as the token option must be preserved so we can't
      # simply overwrite the existing configuration with the given one.
      def runner_update_local(url, token, options)
        index = config_resource_runners.find_index { |r| r['url'].eql?(url) && r['token'].eql?(token) }

        if nil_or_empty?(index, config_resource_runners[index])
          log_chef(:debug, "No runner configuration found for runner with token #{token} at URL #{url}.")
          return
        end

        log_chef(:info, "Checking options for runner #{config_resource_runners[index]['name']} at index #{index}.")

        changed_options = compact_hash!(options).filter do |option, value|
          log_chef(:debug, "Testing option #{option}.")
          if config_resource_runners[index][option].eql?(value)
            log_chef(:debug, "Option #{option} matches. Current: #{config_resource_runners[index][option]}. Incoming: #{value}).")
            false
          else
            log_chef(:debug, "Option #{option} has changed. Current: #{config_resource_runners[index][option]}. Incoming: #{value}.")
            true
          end
        end

        changed_options.each { |option, value| config_resource_runners[index][option] = value } unless nil_or_empty?(changed_options)
        cleaned_options = option_cleanup(config_resource_runners[index], options)

        if nil_or_empty?(changed_options) && !cleaned_options
          log_chef(:info, "Runner #{config_resource_runners[index]['name']} unmodified, options match.")
          false
        else
          log_chef(:info, "Runner #{config_resource_runners[index]['name']} options #{changed_options.keys.join(', ')} updated.") unless nil_or_empty?(changed_options)
          log_chef(:info, "Runner #{config_resource_runners[index]['name']} options #{cleaned_options.join(', ')} removed.") if cleaned_options
          true
        end
      end

      # Convert options hash from 'exec-option' keys to a nested Hash
      def runner_options_to_h(options)
        options_hash = options.dup

        options.each_key do |option|
          next unless runner_type_valid?(option)

          a = runner_option_split(option)
          options_hash[a[0]] ||= {}
          options_hash[a[0]][a[1]] = options_hash.delete(option)
        end

        options_hash
      end

      # Confirm a runner is registered with the host
      def runner_registered?(name = new_resource.name, host = new_resource.url, http_options = new_resource.http_options)
        return false unless runner_configured_for_host?(name, host)

        url, token = runner_get_url_token(name)
        log_chef(:info, "Checking if runner #{name} is registered with #{host}.")

        if api_runner_token_valid?(url, http_options, token)
          log_chef(:info, "Runner #{name} is registered with #{host}.")
          return true
        end

        log_chef(:info, "Runner #{name} is NOT registered with #{host}.")
        false
      end

      # Confirm a runner is configured for the host
      def runner_configured_for_host?(name = new_resource.name, host = new_resource.url)
        url, token = runner_get_url_token(name)

        if !token.nil? && url.eql?(host)
          log_chef(:info, "Runner #{name} is configured for host #{host} with token #{token}.")
          return true
        elsif !token.nil? && !url.eql?(host)
          log_chef(:info, "Runner #{name} is configured with token #{token} but for host #{url} NOT #{host}.")
        else
          log_chef(:info, "Runner #{name} NOT configured for host #{host}.")
        end

        false
      end

      # Register a new runner with the host
      def runner_register(host, http_options, registration_token, options)
        raise ArgumentError, message: 'Registration requires a registration token to be passed' if nil_or_empty?(registration_token)

        log_chef(:info, "Attempting to register runner on #{host} with registation token #{registration_token}.")

        opts = options.dup
        opts['token'] = registration_token
        log_chef(:debug, "Register options: #{option_join(opts)}")

        response = api_runner_register(host, http_options, option_join(opts))

        raise "#{response.code} Error registering: #{api_error_message_output(response)}" unless api_response_is?(response, Net::HTTPCreated)

        response = JSON.parse(response.body)
        log_chef(:info, "Runner #{response.fetch('id')} created sucessfully on #{host} with token #{response.fetch('token')}.")

        [response.fetch('id'), response.fetch('token')]
      end

      # Create a new runner with the host
      def runner_create(host, http_options, token, options)
        raise ArgumentError, message: 'Creation requires an API token to be passed' if nil_or_empty?(token)

        log_chef(:info, "Attempting to create runner on #{host} with API token #{token}.")
        log_chef(:debug, "Create options: #{option_join(options)}")

        response = api_runner_create(host, http_options, token, option_join(options))

        raise "Error creating runner: #{api_error_message_output(response)}" unless api_response_is?(response, Net::HTTPCreated)

        response = JSON.parse(response.body)
        log_chef(:info, "Runner #{response.fetch('id')} created sucessfully on #{host} with token #{response.fetch('token')}.")

        [response.fetch('id'), response.fetch('token')]
      end

      # Unregister a runner from the host
      def runner_unregister!(host, http_options, token)
        log_chef(:info, "Attempting to unregister runner on #{host} with token #{token}.")
        response = api_runner_remove!(host, http_options, token)

        if api_response_is?(response, Net::HTTPNoContent)
          log_chef(:info, "Runner with token #{token} successfully unregistered from #{host}.")
          return true
        end

        log_chef(:error, "Error unregistering runner with token #{token} from #{host}.")
        false
      end

      # Unconfigure a runner from the host
      def runner_unconfigure!(token, url)
        log_chef(:info, "Unconfiguring runner with token #{token} and url #{url}.")
        count_before = config_resource_runners.count
        config_resource_runners.delete_if { |r| r['token'].eql?(token) && r['url'].eql?(url) }

        if config_resource_runners.count < count_before
          log_chef(:info, "Successfully unconfigured runner with token #{token} and url #{url}.")
          true
        else
          log_chef(:error, "Failed to unconfigure runner with token #{token} and url #{url}.")
          false
        end
      end

      # Update a runners options on a host
      def runner_update_server(host, http_options, token, id, options)
        log_chef(:info, "Attempting to update runner #{id} on #{host}.")
        log_chef(:debug, "Update options: #{option_join(options)}")

        update_result = api_runner_update!(host, http_options, token, id, option_join(options))
        if api_response_is?(update_result, Net::HTTPSuccess)
          log_chef(:info, "Runner #{id} updated successfully on #{host}.")
          return true
        end

        log_chef(:error, "Error updating runner #{id} on #{host}, response is #{update_result.code}")

        false
      end

      # Remove options that have been deleted
      def option_cleanup(config, options)
        log_chef(:info, "Performing option cleanup for runner #{config['name']}.")
        removed_options = config.keys.reject { |k| options.key?(k) || RUNNER_LOCAL_PRESERVE.include?(k) }
        if removed_options.empty?
          log_chef(:info, 'Removed options empty, nothing to cleanup.')
          false
        else
          log_chef(:info, "Deleting removed options #{removed_options.join(', ')}.")
          config.reject! { |k, _| removed_options.include?(k) || RUNNER_LOCAL_REMOVE.include?(k) }
          removed_options
        end
      end

      # Join any Array values in Hash
      def option_join(options)
        return options if options.empty?

        options.map { |option, value| [option, value.respond_to?(:join) ? value.join(',') : value] }.to_h
      end

      # Get the runner registration type to be used
      def runner_registration_type
        type = if property_is_set?(:authentication_token)
                 :supplied_token
               elsif property_is_set?(:administration_token)
                 :create
               elsif property_is_set?(:registration_token)
                 :register
               else
                 raise 'Unknown registration type'
               end

        log_chef(:debug) { "Runner registration type is: #{type}" }

        type
      end

      # Option matches a valid runner executor
      def runner_type_valid?(option)
        option.match(RUNNER_EXECUTOR_REGEXP)
      end

      # Split 'executor-option' String into Array of ['executor', 'option']
      def runner_option_split(option)
        option.split(RUNNER_EXECUTOR_REGEXP).delete_if { |string| string.empty? || %w(- _).include?(string) }
      end

      # Split runner configuration file id:name String into Array
      def runner_split_id_name(name)
        split = name.split(':').map(&:strip)
        result = if split.count.eql?(1)
                   [ nil, split.first ]
                 else
                   [ split.first.to_i, split.last ]
                 end

        log_chef(:trace, "Split result: #{result}.")
        result
      end

      # Load the current runner configuration from disk
      def runners_from_disk(config_file = new_resource.config_file)
        toml_load(config_file).fetch('runners', [])
      end

      # Load the current runner configuration from in-flight resource
      def runners_from_resource(config_file = new_resource.config_file)
        find_resource!(:template, config_file).variables.fetch('runners', [])
      end

      # On disk and in-flight configuration
      def runners_config_merged(config_file = new_resource.config_file)
        runners_from_disk(config_file).concat(runners_from_resource(config_file)).uniq { |r| r['name'] }
      end
    end
  end
end
