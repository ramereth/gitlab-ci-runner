require_relative '_common'

module GitlabCiRunner
  module Cookbook
    # General cookbook helpers
    module Helpers
      include GitlabCiRunner::Cookbook::CommonHelpers

      private

      def default_repository_url
        case node['platform_family']
        when 'rhel', 'amazon'
          "#{repo_url_base}/el/#{node['platform_version'].to_i}/$basearch"
        when 'fedora'
          "#{repo_url_base}/fedora/#{node['platform_version'].to_i}/$basearch"
        when 'debian'
          "#{repo_url_base}/#{node['platform']}"
        else
          raise ArgumentError, "#{node['platform_family']} is an unsupported platform family."
        end
      end

      def default_repository_options(repo_url)
        case node['platform_family']
        when 'rhel', 'amazon', 'fedora'
          {
            'baseurl' => repo_url,
            'description' => 'Gitlab CI Runner',
            'gpgkey' => [
              repo_gpg_url,
              'https://packages.gitlab.com/runner/gitlab-runner/gpgkey/runner-gitlab-runner-4C80FB51394521E9.pub.gpg',
              'https://packages.gitlab.com/runner/gitlab-runner/gpgkey/runner-gitlab-runner-49F16C5CC3A0F81F.pub.gpg',
            ],
            'gpgcheck' => true,
          }
        when 'debian'
          {
            'uri' => repo_url,
            'distribution' => debian_codename,
            'components' => %w(main),
            'key' => repo_gpg_url,
          }
        else
          raise ArgumentError, "#{node['platform_family']} is an unsupported platform family."
        end
      end

      def default_gitlab_runner_config_file
        '/etc/gitlab-runner/config.toml'
      end

      def default_gitlab_runner_packages
        %w(gitlab-runner)
      end

      def default_gitlab_runner_user
        'gitlab-runner'
      end

      def default_gitlab_runner_group
        'gitlab-runner'
      end

      def default_gitlab_runner_http_options
        {
          'open_timeout' => 5,
          'read_timeout' => 5,
        }
      end

      def repo_url_base
        'https://packages.gitlab.com/runner/gitlab-runner'
      end

      def repo_gpg_url
        'https://packages.gitlab.com/runner/gitlab-runner/gpgkey'
      end

      def debian_codename
        Mixlib::ShellOut.new('lsb_release -c -s').run_command.stdout.chomp
      end
    end
  end
end
