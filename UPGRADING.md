# Upgrading

This document will give you help on upgrading major versions of gitlab-ci-runner.

## v4.0.0

### Added

- Authentication token support has been added to the runner resource.
  - Registration token support will be removed in Gitlab 18 and will be removed from version 5 of this cookbook.
  - Legacy registration token support must be manually enabled in Gitlab 17.See [Enable runner registrations tokens](https://docs.gitlab.com/ee/administration/settings/continuous_integration.html#enable-runner-registrations-tokens) for details.

## v3.0.0

### Added

- load_current_value and converge_if_changed support
  - global resource
  - runner resource

### Changed

- Following properties are now required for the runner resource:
  - `:url`
  - `:registration_token` - Only with the `:register` action
